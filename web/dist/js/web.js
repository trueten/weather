var map;
var weatherBox = new Array();
var t0;
var t1;
var duration;

function initMap() {

    var positionMap = { lat: 52.248, lng: 21.013 };

    map = new google.maps.Map(document.getElementById('map'), {
        center: positionMap,
        zoom: 8
    });

    var marker = new google.maps.Marker({
        position: positionMap,
        map: map
    });

    map.addListener('click', function (e) {
        t0 = performance.now();
            var lat = e.latLng.lat();
            var lng = e.latLng.lng();
            blockPage();
            placeMarkerAndPanTo(e.latLng, map);        
            getWeather(lat, lng);
    });

    function placeMarkerAndPanTo(latLng, map) {
        marker.setMap(null);
        marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
        map.panTo(latLng);
    }
}

function getWeather(latitude, longitude) {
    weatherBox['lat'] = latitude;
    weatherBox['lng'] = longitude;
    
    $.getJSON("http://api.openweathermap.org/data/2.5/find?lat=" + latitude + "&lon=" + longitude + "&cnt=1&units=metric&lang=pl&appid=765557fd6310de56497f48e5d364fbe0")
    .done(function (data) {
        $.each(data.list, function (i, item) {
            weatherBox['city'] = String(item.name);
            weatherBox['temp'] = parseInt(item.main.temp);
            weatherBox['clouds'] = parseInt(item.clouds.all);
            weatherBox['wind'] = String('Z prędkością ' + item.wind.speed + ' w kierunku ' + item.wind.deg);
            weatherBox['desc'] = item.weather[0].description;
        });
    })
    .fail(function () {
        alert("error");
    })
    .always(function () {
        sendWeatherData(weatherBox);
     });

    return true;
}

function sendWeatherData() {

    t1 = performance.now();
    duration = t1 - t0;

    fileData = {
        lat: weatherBox.lat,
        lng: weatherBox.lng,
        city: weatherBox.city,
        temp: weatherBox.temp,
        clouds: weatherBox.clouds,
        wind: weatherBox.wind,
        desc: weatherBox.desc,
        duration: duration
    }
    
    $.ajax({
        type: 'POST',
        data: fileData,
        dataType: "json",
        url: '/app_dev.php/ajax/save/history',
        success: function (response) {
            unblockPage();
            showModal(response);
        },
        error: function (response) {
            alert('sorry for inconvinience');
        }
    });
}

function blockPage() {
    $.blockUI({ css: { backgroundColor: '#f00', color: '#fff' } });
}

function unblockPage() {
    $.unblockUI();
}

function showModal(data) {
    var html = '<div class="modal">';
    
    html += '<span>Miejscowość: ' + data.city + '</span><br />';
    html += '<span>Temperatura:' + data.temp +' °C</span><br />';
    html += '<span>Zachmurzenie: ' + data.clouds +'%</span><br />';
    html += '<span>Wiatr: ' + data.wind +'</span><br />';
    if((data.desc).length > 0) {
        html += '<span>Opis: '+data.desc+'</span><br />';
    }
    html += '</p>';
    html += '<p>';
    html += '<br /><span>Zapytanie trwało: '+ data.duration +' ms</span>';
    html += '</p>';
    html += '</div>';

    $(html).appendTo('body').modal();
}