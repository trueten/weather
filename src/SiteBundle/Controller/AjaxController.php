<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Admin\MapsBundle\Entity\MapsHistory;

class AjaxController extends Controller
{
    /**
     * @Route("/ajax/save/history", condition="request.isXmlHttpRequest()")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function ajaxMapsAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $maps = $this->getRequest()->request->all();

        $mapsHistory = new MapsHistory();
        $mapsHistory->setLattitude($maps['lat']);
        $mapsHistory->setLongitude($maps['lng']);
        $mapsHistory->setCity($maps['city']);
        $mapsHistory->setTemperature($maps['temp']);
        $mapsHistory->setCloudy($maps['clouds']);
        $mapsHistory->setWind($maps['wind']);
        if( isset($maps['desc']) && $maps['desc'] != '') {
            $mapsHistory->setDescription($maps['desc']);
        }
        $mapsHistory->setSearchDuration($maps['duration']);

        $em->persist($mapsHistory);
        $em->flush();

        
        $response = json_encode(array('success' => true));
        return new Response(json_encode($maps));
    }
}
