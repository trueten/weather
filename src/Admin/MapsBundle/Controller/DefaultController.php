<?php

namespace Admin\MapsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $mapsRepository = $em->getRepository('MapsBundle:MapsHistory');

        $lastItems = $mapsRepository->findBy([], ['createDate' => 'DESC'], 10, null);

        //$high_temp = $mapsRepository->findOneBy([], ['temperature' => 'DESC']);
        $high_temp = $em->createQueryBuilder()
            ->select('MAX(e.temperature)')
            ->from('MapsBundle:MapsHistory', 'e')
            ->getQuery()
            ->getSingleScalarResult();

        $low_temp = $em->createQueryBuilder()
            ->select('MIN(e.temperature)')
            ->from('MapsBundle:MapsHistory', 'e')
            ->getQuery()
            ->getSingleScalarResult();

        $avg_temp = $em->createQueryBuilder()
            ->select('AVG(e.temperature)')
            ->from('MapsBundle:MapsHistory', 'e')
            ->getQuery()
            ->getSingleScalarResult();

        $count = $em->createQueryBuilder()
            ->select('count(elements.id)')
            ->from('MapsBundle:MapsHistory','elements')
            ->getQuery()
            ->getSingleScalarResult();

        $citySum = $em->createQueryBuilder()
            ->select(array('c.city, count(c) as cityCount'))
            ->from('MapsBundle:MapsHistory','c')
            ->groupBy('c.city')
            ->orderBy('cityCount', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();           

        return $this->render('MapsBundle:Default:index.html.twig', [
            'lastItems' => $lastItems,
            'high_temp' => $high_temp,
            'low_temp' => $low_temp,
            'avg_temp' => $avg_temp,
            'citySum' => $citySum[0],
            'count' => $count
        ]);
    }

    
}
