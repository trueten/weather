<?php

namespace Admin\MapsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * MapsHistory
 *
 * @ORM\Table(name="maps_history")
 * @ORM\Entity(repositoryClass="Admin\MapsBundle\Repository\MapsHistoryRepository")
 * @UniqueEntity("id")
 */
class MapsHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lattitude;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     */
    private $temperature;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cloudy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wind;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $searchDuration;

    /**
     * @ORM\Column(type="datetime", length=60, nullable=true)
     */
    private $createDate;


    public function __construct() {
        $this->createDate = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lattitude
     *
     * @param string $lattitude
     * @return MapsHistory
     */
    public function setLattitude($lattitude)
    {
        $this->lattitude = $lattitude;

        return $this;
    }

    /**
     * Get lattitude
     *
     * @return string
     */
    public function getLattitude()
    {
        return $this->lattitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return MapsHistory
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return MapsHistory
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set temperature
     *
     * @param string $temperature
     * @return MapsHistory
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * Get temperature
     *
     * @return integer
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Set cloudy
     *
     * @param string $cloudy
     * @return MapsHistory
     */
    public function setCloudy($cloudy)
    {
        $this->cloudy = $cloudy;

        return $this;
    }

    /**
     * Get cloudy
     *
     * @return string
     */
    public function getCloudy()
    {
        return $this->cloudy;
    }

    /**
     * Set wind
     *
     * @param string $wind
     * @return MapsHistory
     */
    public function setWind($wind)
    {
        $this->wind = $wind;

        return $this;
    }

    /**
     * Get wind
     *
     * @return string
     */
    public function getWind()
    {
        return $this->wind;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return MapsHistory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set searchDuration
     *
     * @param string $searchDuration
     * @return MapsHistory
     */
    public function setSearchDuration($searchDuration)
    {
        $this->searchDuration = $searchDuration;

        return $this;
    }

    /**
     * Get searchDuration
     *
     * @return string
     */
    public function getSearchDuration()
    {
        return $this->searchDuration;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return MapsHistory
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }
}

